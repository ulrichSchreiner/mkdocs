.PHONY:
build:
	docker build -t registry.gitlab.com/ulrichschreiner/mkdocs:latest .

.PHONY:
force:
	docker build --no-cache -t registry.gitlab.com/ulrichschreiner/mkdocs:latest .

