FROM alpine:3

RUN apk update && \
    apk add \
        bash \
        build-base \
        gcc \
        git \
        musl-dev \
        pango \
        py3-pip \
        py3-brotli \
        py3-cffi \
        py3-pillow \
        py3-pygments \
        python3-dev && \
    pip install \
        mkdocs \
        mkdocs-include-markdown-plugin \
        mkdocs-kroki-plugin \
        mkdocs-macros-plugin \
        mkdocs-material \
        mkdocs-literate-nav \
        mkdocs-print-site-plugin \
        mkdocs-with-pdf \
        pymdown-extensions \
        weasyprint --break-system-packages && \
    mkdir /docs

WORKDIR /docs

CMD ["/usr/bin/mkdocs"]
